<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored = "false" %>
<html>
<head>
    <title>Shop list</title>
</head>
<body>
<%! private ArrayList<String> zakupy = new ArrayList<String>
        (Arrays.asList("ekspresy wspinaczkowe", "karabinek HMS", "kask"));%>
<% request.setAttribute("zakupy", zakupy); %>
<% for (String product : zakupy) {
    out.println(product + "<br>");
}
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:forEach var="i" begin="0" end="${zakupy.size()-1}">
    item: <c:out value="${zakupy.get(i)}"></c:out><br>
</c:forEach>
<c:forEach items="${zakupy}" var="item">
    ${item}<br>
</c:forEach>
</body>
</html>
