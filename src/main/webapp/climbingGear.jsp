<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Climbing Gear</title>
    <link rel="stylesheet" href="CSS_01.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
<div>
    <p>Sprzęt wspinaczkowy</p>
    <div class="row">
        <div class="col-4">
            <div class="list-group" id="list-tab" role="tablist">
                <a class="list-group-item list-group-item-action" id="list-first-list" data-toggle="list"
                   href="toogleCategory?id=0#list-first" role="tab" aria-controls="first">Ekspresy wspinaczkowe</a>
                <a class="list-group-item list-group-item-action" id="list-second-list" data-toggle="list"
                   href="toogleCategory?id=1#list-second" role="tab" aria-controls="second">Przyrząd do asekuracji</a>
                <a class="list-group-item list-group-item-action" id="list-third-list" data-toggle="list"
                   href="toogleCategory?id=2#list-third" role="tab" aria-controls="third">Karabinek</a>
                <a class="list-group-item list-group-item-action" id="list-fourth-list" data-toggle="list"
                   href="toogleCategory?id=3#list-fourth" role="tab" aria-controls="fourth">Lina</a>
                <a class="list-group-item list-group-item-action" id="list-fifth-list" data-toggle="list"
                   href="toogleCategory?id=4#list-fifth" role="tab" aria-controls="fifth">Kask</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>
