<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Produkty</title>
    <link rel="stylesheet" href="CSS_00.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
<p>Wybierz kategorie produktu</p>
<div>
<form action="products" method="POST">
    <label for="kategoria"> kategoria: </label>
    <select id=kategoria name=kategoria>
        <option value="odziez damska">Odzież damska</option>
        <option value="odziez meska">Odzież męska</option>
        <option value="sprzet wspinaczkowy">Sprzęt wspinaczkowy</option>
        <option value="buty">Buty</option>
        </select> <br>
        <input type="submit" value="wybierz">
</form>
</div>
</body>
</html>
